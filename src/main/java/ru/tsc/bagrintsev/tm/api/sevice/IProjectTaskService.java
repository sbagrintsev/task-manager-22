package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public interface IProjectTaskService extends Checkable {

    void bindTaskToProject(String userId, String projectId, String taskId) throws AbstractException;

    void unbindTaskFromProject(String userId, String projectId, String taskId) throws AbstractException;

    void removeProjectById(String userId, String projectId) throws AbstractException;

}
