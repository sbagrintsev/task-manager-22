package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;
import java.util.Map;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    void setProjectId(String userId, String taskId, String projectId) throws AbstractException;

}
