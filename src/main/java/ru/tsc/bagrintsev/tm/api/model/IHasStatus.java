package ru.tsc.bagrintsev.tm.api.model;

import ru.tsc.bagrintsev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
