package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    M add(String userId, M record) throws AbstractException;

    List<M> findAll(final String userId) throws AbstractException;

    List<M> findAll(final String userId, final Comparator<M> comparator) throws AbstractException;

    M findOneByIndex(final String userId, final Integer index) throws AbstractException;

    M findOneById(final String userId, final String id) throws AbstractException;

    boolean existsById(final String userId, final String id) throws AbstractException;

    M remove(final String userId, final M record) throws AbstractException;

    M removeByIndex(final String userId, final Integer index) throws AbstractException;

    M removeById(final String userId, final String id) throws AbstractException;

    long totalCount(final String userId) throws AbstractException;

    void clear(final String userId) throws AbstractException;
}
