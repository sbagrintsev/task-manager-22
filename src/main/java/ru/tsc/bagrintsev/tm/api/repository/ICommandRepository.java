package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name) throws CommandNotSupportedException;

    AbstractCommand getCommandByShort(String shortName) throws ArgumentNotSupportedException;

    Collection<AbstractCommand> getAvailableCommands();

}
