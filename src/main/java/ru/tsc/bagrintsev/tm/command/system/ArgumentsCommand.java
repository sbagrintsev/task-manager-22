package ru.tsc.bagrintsev.tm.command.system;

import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[CommandLine arguments]");
        Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.stream()
                .filter(c -> c.getShortName() != null && !c.getShortName().isEmpty())
                .forEach(c -> System.out.printf("%-35s%s\n", c.getShortName(), c.getDescription()));
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public String getShortName() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Print command-line arguments.";
    }

}
