package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

}
