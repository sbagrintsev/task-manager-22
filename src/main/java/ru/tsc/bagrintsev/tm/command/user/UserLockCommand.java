package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public final class UserLockCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "Admin functional: lock user.";
    }
}
