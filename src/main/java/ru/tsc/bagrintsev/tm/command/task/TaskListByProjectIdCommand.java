package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        int index = 0;
        for (final Task task : tasks) {
            System.out.println(++index + ". " + task);
        }
    }

    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Override
    public String getDescription() {
        return "List tasks by project id.";
    }
}
