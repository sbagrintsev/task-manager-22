package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAR]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
