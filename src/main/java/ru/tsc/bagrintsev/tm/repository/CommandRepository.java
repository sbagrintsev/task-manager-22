package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> names = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> shortNames = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        Optional.ofNullable(command.getName()).ifPresent(n -> names.put(n, command));
        Optional.ofNullable(command.getShortName()).ifPresent(n -> shortNames.put(n, command));
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws CommandNotSupportedException {
        final Optional<AbstractCommand> command = Optional.ofNullable(names.get(name));
        command.orElseThrow(() -> new CommandNotSupportedException(name));
        return command.get();
    }

    @Override
    public AbstractCommand getCommandByShort(final String shortName) throws ArgumentNotSupportedException {
        final Optional<AbstractCommand> command = Optional.ofNullable(shortNames.get(shortName));
        command.orElseThrow(() -> new ArgumentNotSupportedException(shortName));
        return command.get();
    }

    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return names.values();
    }

}
