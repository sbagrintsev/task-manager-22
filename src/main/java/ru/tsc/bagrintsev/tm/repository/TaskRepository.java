package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;


public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(userId, task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return records.stream()
                .filter(record -> projectId.equals(record.getProjectId()) && userId.equals(record.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void setProjectId(final String userId, final String taskId, final String projectId) throws AbstractException {
        final Task task = findOneById(userId, taskId);
        task.setProjectId(projectId);
    }

}
