package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PermissionDeniedException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class AuthService implements IAuthService{

    private final IUserService userService;

    public String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void signIn(final String login, final String password) throws AbstractException, GeneralSecurityException {
        check(EntityField.LOGIN, login);
        check(EntityField.PASSWORD, password);
        final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.generateHash(password, user.getPasswordSalt()))) {
            throw new PasswordIsIncorrectException();
        }
        userId = user.getId();
    }

    @Override
    public void signOut() {
        userId = null;
    }

    @Override
    public String getCurrentUserId() {
        return userId;
    }

    @Override
    public User getCurrentUser() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(final Role[] roles) throws AbstractException {
        if (roles.length == 0) return;
        final User user = getCurrentUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionDeniedException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionDeniedException();
    }
}
