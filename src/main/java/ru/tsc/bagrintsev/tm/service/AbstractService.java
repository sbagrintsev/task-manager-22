package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IAbstractService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.*;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M record) throws AbstractEntityException {
        check(Entity.ABSTRACT, record);
        return repository.add(record);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        if (comparator == null) {
            return findAll();
        }
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return findAll(sort.getComparator());
    }

    @Override
    public void check(Integer index) throws IncorrectIndexException {
        if (index == null ||
                index < 0 ||
                index > repository.totalCount()) throw new IncorrectIndexException();
    }

    @Override
    public M findOneByIndex(final Integer index) throws IncorrectIndexException {
        check(index);
        return repository.findOneByIndex(index);
    }

    @Override
    public M findOneById(final String id) throws AbstractException {
        check(EntityField.ID, id);
        return repository.findOneById(id);
    }

    @Override
    public boolean existsById(String id) throws AbstractException {
        return repository.existsById(id);
    }

    @Override
    public M removeByIndex(final Integer index) throws AbstractException {
        check(index);
        return repository.removeByIndex(index);
    }

    @Override
    public M removeById(final String id) throws AbstractException {
        check(EntityField.ID, id);
        return repository.removeById(id);
    }

    @Override
    public int totalCount() {
        return repository.totalCount();
    }

    @Override
    public M remove(final M record) throws AbstractException {
        check(Entity.ABSTRACT, record);
        return repository.remove(record);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void removeAll(Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}
