package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;


    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return commandRepository.getAvailableCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws AbstractException {
        check(EntityField.COMMAND_NAME, name);
        final Optional<AbstractCommand> command = Optional.ofNullable(commandRepository.getCommandByName(name));
        command.orElseThrow(() -> new CommandNotSupportedException(name));
        return command.get();
    }

    @Override
    public AbstractCommand getCommandByShort(final String shortName) throws AbstractException {
        check(EntityField.COMMAND_SHORT, shortName);
        final Optional<AbstractCommand> command = Optional.ofNullable(commandRepository.getCommandByShort(shortName));
        command.orElseThrow(() -> new ArgumentNotSupportedException(shortName));
        return command.get();
    }

}
