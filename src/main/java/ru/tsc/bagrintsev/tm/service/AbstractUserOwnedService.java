package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IUserOwnedService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;
import ru.tsc.bagrintsev.tm.repository.AbstractUserOwnedRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M,R> implements IUserOwnedService<M> {


    public AbstractUserOwnedService(R repository) {
        super(repository);
    }

    @Override
    public M add(String userId, M record) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        return repository.add(userId, record);
    }

    @Override
    public List<M> findAll(String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(String userId, Comparator<M> comparator) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (comparator == null) {
            return findAll(userId);
        }
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(String userId, Sort sort) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (sort == null) {
            return findAll(userId);
        }
        return findAll(userId, sort.getComparator());
    }

    @Override
    public M findOneByIndex(String userId, Integer index) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public M findOneById(String userId, String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        return repository.findOneById(userId, id);
    }

    @Override
    public boolean existsById(String userId, String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        return repository.existsById(userId, id);
    }

    @Override
    public M remove(String userId, M record) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        return repository.remove(userId, record);
    }

    @Override
    public M removeByIndex(String userId, Integer index) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        return repository.removeByIndex(userId, index);
    }

    @Override
    public M removeById(String userId, String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        return repository.removeById(userId, id);
    }

    @Override
    public long totalCount(String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        return repository.totalCount(userId);
    }

    @Override
    public void clear(String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        repository.clear(userId);
    }

}
