package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.exception.user.EmailAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public UserService(
            final IUserRepository repository,
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        return repository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) throws AbstractException {
        check(EntityField.EMAIL, email);
        return repository.findByEmail(email);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        try {
            findByLogin(login);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        try {
            findByEmail(email);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        final User user =  repository.removeByLogin(login);
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    public User create(final String login, final String password) throws GeneralSecurityException, AbstractException {
        check(EntityField.LOGIN, login);
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        check(EntityField.PASSWORD, password);
        return repository.create(login, password);
    }

    @Override
    public User setParameter(final User user,
                             final EntityField paramName,
                             final String paramValue) throws AbstractException {
        check(Entity.USER, user);
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) throw new EmailAlreadyExistsException(paramValue);
        return repository.setParameter(user, paramName, paramValue);
    }

    @Override
    public User setRole(final User user, final Role role) throws AbstractException {
        check(Entity.USER, user);
        check(Entity.ROLE, role);
        return repository.setRole(user, role);
    }

    @Override
    public User setPassword(final String userId, final String password) throws AbstractException, GeneralSecurityException {
        check(EntityField.ID, userId);
        check(EntityField.PASSWORD, password);
        final User user = findOneById(userId);
        repository.setUserPassword(user, password);
        return user;
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws AbstractException {
        check(EntityField.ID, userId);
        final User user = findOneById(userId);
        setParameter(user, EntityField.FIRST_NAME, firstName);
        setParameter(user, EntityField.MIDDLE_NAME, middleName);
        setParameter(user, EntityField.LAST_NAME, lastName);
        return user;
    }

    @Override
    public void lockUserByLogin(String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        final User user = repository.findByLogin(login);
        repository.setParameter(user, EntityField.LOCKED, "true");
    }

    @Override
    public void unlockUserByLogin(String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        final User user = repository.findByLogin(login);
        repository.setParameter(user, EntityField.LOCKED, "false");
    }

}
