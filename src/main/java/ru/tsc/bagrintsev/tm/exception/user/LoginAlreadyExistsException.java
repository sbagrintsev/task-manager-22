package ru.tsc.bagrintsev.tm.exception.user;

import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;

public final class LoginAlreadyExistsException extends AbstractUserException {

    public LoginAlreadyExistsException(final String login) {
        super(String.format("Error! Login %s is already in use...", login));
    }

}
