package ru.tsc.bagrintsev.tm.exception.user;

import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;

public final class EmailAlreadyExistsException extends AbstractUserException {

    public EmailAlreadyExistsException(final String email) {
        super(String.format("Error! Email %s is already in use...", email));
    }

}
