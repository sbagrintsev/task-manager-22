package ru.tsc.bagrintsev.tm.exception.user;

import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;

public final class PasswordIsIncorrectException extends AbstractUserException {

    public PasswordIsIncorrectException() {
        super("Error! Wrong password...");
    }

}
